var resultButton = document.getElementById("result");

var paspaudimai = document.getElementsByClassName("atsakymas");

var pasirinkimai = [];

var klausimai = [
    {
        klausimas: "1. Daugiausia gėlo vandens Žemėje susitelkę:",
        atsakymai: [
            "Požeminiuose vandenyse",
            "Upėse",
            "Ledynuose ir sniegynuose",
            "Ežeruose"
        ],
        teisingasAtsakymas: 2,
    },
    {
        klausimas: "2. Kokios kilmės buvo Fernandas Magelanas?",
        atsakymai: [
            "Portugalas",
            "Ispanas",
            "Italas",
            "Anglas"
        ],
        teisingasAtsakymas: 0,
    },
    {
        klausimas: "3. Vėjas pučia:",
        atsakymai: [
            "Iš žemesnio slėgio srities į aukštesnio slėgio sritį",
            "Iš aukštesnio slėgio srities į žemesnio slėgio sritį",
            "Tarp vienodo slėgio sričių",
            "Iš aukštesnio slėgio srities į vidutinio slėgio sritį"
        ],
        teisingasAtsakymas: 1,
    },
    {
        klausimas: "4. Pagal Boforto skalę vertinamas:",
        atsakymai: [
            "Vėjo stiprumas",
            "Žemės drebėjimų stiprumas",
            "Jūros bangavimas",
            "Ūragano stiprumas"
        ],
        teisingasAtsakymas: 0,
    },
    {
        klausimas: "5. Kas vadinama Okeanija?",
        atsakymai: [
            "Salų pasaulis Ramiajame vandenyne",
            "Senovinis žemynas",
            "Vandenynų dugnas",
            "Atsiskyrusi Jūra"
        ],
        teisingasAtsakymas: 0,
    },
    {
        klausimas: "6. Kurios Europos Sąjungos šalies vėliavoje vaizduojama tos šalies sala?",
        atsakymai: [
            "Kipro",
            "Maltos",
            "Airijos",
            "Anglijos"
        ],
        teisingasAtsakymas: 0,
    },
    {
        klausimas: "7. Vietos dykumoje, kur gyvybei egzistuoti padeda požeminis arba upių vanduo, yra: ",
        atsakymai: [
            "Vadės",
            "Oazės",
            "Savanos",
            "Kanalai"
        ],
        teisingasAtsakymas: 1,
    },
    {
        klausimas: "8. Vienas iš šių rodiklių nėra vaizduojamas klimatogramoje:",
        atsakymai: [
            "Temperatūros kreivė",
            "Aukštis virš jūros lygio",
            "Vidutinė metų oro temperatūra",
            "Vidutinis oro slėgis"
        ],
        teisingasAtsakymas: 3,
    },
    {
        klausimas: "9. Kuris įžymus objektas (statinys) yra Indijoje? ",
        atsakymai: [
            "Acino",
            "Marques De Riscal",
            "TadžMahalas",
            "Urbis"
        ],
        teisingasAtsakymas: 2,
    },
    {
        klausimas: "10. Kurios iš šių valstybių valdymo forma nėra monarchija? ",
        atsakymai: [
            "Kanada",
            "Jamaika",
            "Portugalija",
            "Japonija"
        ],
        teisingasAtsakymas: 2,
    }
];
 
// ikelia i html klausimus-atsakymus
function klausimaiAtsakymai() {
    var k = 0;
    for (var i = 0; i < klausimai.length; i++) {
        document.getElementsByClassName("klausimas")[i].innerHTML = klausimai[i].klausimas;
        for (var j = 0; j < klausimai[i].atsakymai.length; j++) {
            document.getElementsByClassName("atsakymas")[j + k].innerHTML = klausimai[i].atsakymai[j];
        }
        k += 4;
    }
}
 
//rodyti instant quiz table
klausimaiAtsakymai();

    // sukuriamas ciklas paspaudimams
    for (var i = 0; i < paspaudimai.length; i++) {
 
        paspaudimai[i].addEventListener("click", function () { pasirinktiAtsakyma(event) });
    }
 
// Sukuriami ciklai pasirinkto atsakymo spalvai uzdeti ir patikrinti
function pasirinktiAtsakyma(e) {
 
    for (var i = 0; i < klausimai.length; i++) {
        if (klausimai[i].atsakymai.indexOf(e.currentTarget.innerText) > -1) {
 
            for (var j = 0; j < klausimai[i].atsakymai.length; j++) {
                paspaudimai[j + i * 4].classList.remove("spalva");
                console.log(j + i * 4);
            }
        pasirinkimai[i] = e.currentTarget.innerText;
       
        e.currentTarget.classList.add("spalva");
        }
    }
}
 
// sukuriami ciklai, kad paspaudus pateiktu rezultatus
resultButton.addEventListener("click", function () {
    var rezultatas = 0;
    for (var i = 0; i < klausimai.length; i++) {
        for (var j = 0; j < klausimai[i].atsakymai.length; j++) {
           
            if (klausimai[i].atsakymai[klausimai[i].teisingasAtsakymas] == pasirinkimai[i]) {
                rezultatas += 1;
            } 
            j += 4;
        }
    }
    
    if(rezultatas >= 5) {
    swal("Sveikiname!", "Surinkti taskai: "  + rezultatas + " iš 10", "success");
    } else if (rezultatas >= 2){
    swal("Pabandykite dar karta", "Surinkti taskai: "  + rezultatas + " iš 10", "warning");
    } else if (rezultatas <= 1){
    swal("Oops", "Surinkti taskai: "  + rezultatas + " iš 10", "error");    
    }
    
});