var kursai = {
  USD: 1.1432,
  EUR: 0.8686
}

var kiekisInput = document.getElementById("kiekis");
var gaunuInput = document.getElementById("kiekisGaunu");

//kiekisInput.value = 500;

kiekisInput.addEventListener("input", function() {
  skaiciuotiKursa();
});

function skaiciuotiKursa() {
	var parduoduValiuta = document.getElementById("parduodu");
    var perkuValiuta = document.getElementById("perku");
	
	if(parduoduValiuta.value == perkuValiuta.value ) {
		alert("Negalite keisti tos pacios valiutos");
	} else {
		var valiuta;
    	if(parduoduValiuta.value == "USD") {
      		valiuta = kiekisInput.value * kursai[perkuValiuta.value];
      		gaunuInput.value = valiuta;  
    } else if(parduoduValiuta.value == "EUR") {
    	valiuta = kiekisInput.value * kursai[perkuValiuta.value];
      	gaunuInput.value = valiuta;  
    }
}
}
