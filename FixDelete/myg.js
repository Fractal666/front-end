$(document).ready(function() {
  // Kai visas html susigeneravo ir yra pasiruose darbui
  
});

$(window).load(function() {
    // sitas kodas suveikia tada, kai uzsikrove VISI resursai (css, js, paveiksliukai ir tt)
});

var automobiliai = [
  {
    data: "2016-09-13",
    numeriai: "ABC 444",
    atstumas: 2000,
    laikas: 500
  }
];

var eiluciuSkaicius = 0;



pridetiEilute(automobiliai[0]);

$(".naujas").click(function() {
  // Parodome modalini langa
  $('#naujoIvedimas').modal('show');

});

$(".prideti").click(function() {
  var automobilis = {};
  automobilis.data = $(".dataInput").val();
  automobilis.numeriai = $(".numeriaiInput").val();
  automobilis.atstumas = $(".atstumasInput").val();
  automobilis.laikas = $(".laikasInput").val();
  automobiliai.push(automobilis);
  
  pridetiEilute(automobilis);
  $('#naujoIvedimas').modal('hide');
});

function pridetiEilute(automobilis) {
  eiluciuSkaicius++;
  var eilute = "<tr data-eilutesnr='" + eiluciuSkaicius +"' >";
  eilute += "<td>" + automobilis.data + "</td>";
  eilute += "<td>" + automobilis.numeriai + "</td>";
  eilute += "<td>" + automobilis.atstumas + "</td>";
  eilute += "<td>" + automobilis.laikas + "</td>";
  eilute += "<td class='naikinti'>Naikinti</td>";
  eilute += "<td class='edit'>Taisyti</td>";

  eilute += "</tr>";
   
  $(".masinos").append(eilute);
  
  $(".naikinti").click(function() {
    $(this).parent().remove();
  });
  
  $(".edit").click(function() {
    $('#naujoIvedimas').modal('show');
    // Cia gaunu tr elemento data atributa pavadinimu eilutesnr
    var eilute = $(this).parent().data("eilutesnr");
    alert(eilute);
    
    // Cia gaunu 3 paspaustos eilutes TD elemento reiksme
    var data = $(this).parent().find("td:nth-child(3)").text();
    alert(data);
    $(".dataInput").val(data);
  });
}